#!/bin/bash

#------------------------------- DEFINE VARIABLES -------------------------------#
pacman_args=
dependencies_dir=$HOME/dep
#--------------------------------------------------------------------------------#


#----------------------------- INSTALL DEPENDENCIES -----------------------------#
declare -a packages=$(cat ./packages.txt)
for package in ${packages[*]}; do
  if ! [[ $package =~ \#.* ]]; then
    pacman_args+=$package
    pacman_args+=" "
  fi
done

sudo pacman -Sy $pacman_args
#--------------------------------------------------------------------------------#


#----------------------------------- LIGHTDM ------------------------------------#
sudo systemctl enable lightdm.service
sudo sed -i 's/^\[Seat:\*\]$/[Seat:*]\ndisplay-setup-script=xrandr -s 1920x1080 --dpi 200/g' /etc/lightdm/lightdm.conf
#--------------------------------------------------------------------------------#


#------------------------------------- YAY --------------------------------------#
git clone https://aur.archlinux.org/yay.git $dependencies_dir/yay
cd $dependencies_dir/yay
yes | makepkg -sirc
cd $HOME
#--------------------------------------------------------------------------------#


#------------------------------------ BRAVE -------------------------------------#
printf "N\n" | yay -Sy brave-bin
#--------------------------------------------------------------------------------#


#------------------------------------ FONTS -------------------------------------#
  # FiraCode
mkdir -p $dependencies_dir/fonts
cd $dependencies_dir/fonts
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraMono.zip
unzip FiraMono.zip
rm FiraMono.zip
sudo mkdir -p /usr/share/fonts/OTF
sudo cp Fira\ * /usr/share/fonts/OTF/

  # mononoki
printf "N\n" | yay -Sy nerd-fonts-mononoki

  # refresh font cache
fc-cache
cd $HOME
#--------------------------------------------------------------------------------#


#----------------------------------- DOTFILES -----------------------------------#
git clone https://gitlab.com/sonderpoke/dotfiles $HOME/dotfiles
#--------------------------------------------------------------------------------#


#---------------------------------- ALACRITTY -----------------------------------#
mkdir -p $HOME/.config/alacritty
cp $HOME/dotfiles/alacritty/alacritty.yml $HOME/.config/alacritty/
#--------------------------------------------------------------------------------#


#------------------------------------- ZSH --------------------------------------#
sudo chsh $USER -s /bin/zsh
cp $HOME/dotfiles/.zshrc $HOME
sudo cp $HOME/dotfiles/environment /etc/environment
git clone https://github.com/agnoster/agnoster-zsh-theme $HOME/.config/zsh/agnoster-zsh-theme
#--------------------------------------------------------------------------------#


#------------------------------------ NEOVIM ------------------------------------#
git clone https://github.com/neovim/neovim.git $dependencies_dir/neovim
cd $dependencies_dir/neovim/
sudo make install
cp -r $HOME/dotfiles/nvim $HOME/.config/
#--------------------------------------------------------------------------------#


#------------------------------------- TMUX -------------------------------------#
cp -r $HOME/dotfiles/tmux $HOME/.config/
#--------------------------------------------------------------------------------#


#-------------------------------------- i3 --------------------------------------#
cp -R $HOME/dotfiles/i3 $HOME/.config/
#--------------------------------------------------------------------------------#


#--------------------------- NITROGEN AND WALLPAPERS ----------------------------#
cp -r $HOME/dotfiles/Pictures $HOME/
cp -r $HOME/dotfiles/nitrogen $HOME/.config/
sed -i "s/USER/$USER/g" $HOME/.config/nitrogen/bg-saved.cfg
sed -i "s/USER/$USER/g" $HOME/.config/nitrogen/nitrogen.cfg
#--------------------------------------------------------------------------------#


#--------------------------------- CLEAR CACHE ----------------------------------#
yes | yay -Sc
#--------------------------------------------------------------------------------#


#---------------------------- REMOVE DEPENDENCY DIR -----------------------------#
rm -rdf $dependencies_dir
#--------------------------------------------------------------------------------#

